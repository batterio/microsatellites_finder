__author__ = 'Loris Mularoni'


# Import modules
import unittest
from micros_finder import utils


class TestLength1(unittest.TestCase):

    def test_repeat_3_to_6_perfect_success(self, length=1, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_1', 'ACTGAAAAAGAAGCAACGTA'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [['test_1', 4, 9, 'A', 'AAAAA']])

    def test_repeat_6_to_10_perfect_fail(self, length=1, alphabet='dna'):
        """Test fails because the microsatellites are not long enough"""
        for min_repetitions in (6, 10, 15):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_2', 'ACTGAAAAAGAAGCAACGTA'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [])

    def test_repeat_3_to_6_imperfect_success(self, length=1, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=0,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_3', 'ACTGAAAAAGAAGCAACGTA'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [['test_3', 4, 9, 'A', 'AAAAA']])
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_3', 'ACTGAAAAAGAAGCAACGTA'))
            matches = micro.find_microsatellites()
            if min_repetitions == 3:
                self.assertEqual(matches, [['test_3', 4, 12, 'A', 'AAAAAGAA']])
            else:
                self.assertEqual(matches, [['test_3', 4, 9, 'A', 'AAAAA']])
        for min_repetitions in range(3, 4):
            for imperfect in range(2, 6):
                micro = utils.Microsatellite(length, min_repetitions, imperfect=imperfect,
                                             strict=False, flanks=0, alphabet=alphabet)
                micro.load_sequence(('test_3', 'ACTGAAAAAGAAGCAACGTA'))
                matches = micro.find_microsatellites()
                self.assertEqual(matches, [['test_3', 4, 16, 'A', 'AAAAAGAAGCAA']])


class TestLength2(unittest.TestCase):

    def test_repeat_3_to_6_perfect_success(self, length=2, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_4', 'ACTGTGTGTGTGGTGTGAAGTGTCAACGTA'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [['test_4', 2, 12, 'GT', 'TGTGTGTGTG']])

    def test_repeat_6_to_10_perfect_fail(self, length=2, alphabet='dna'):
        """Test fails because the microsatellites are not long enough"""
        for min_repetitions in (6, 10, 15):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_5', 'ACTGTGTGTGTGGTGTGAAGTGTCAACGTA'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [])

    def test_repeat_3_to_6_imperfect_success(self, length=2, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=0,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_6', 'ACTGTGTGTGTGGTGTGAAGTGTCAACGTA'))
            matches = micro.find_microsatellites()
            if min_repetitions == 3:
                self.assertEqual(matches, [['test_6', 2, 17, 'GT', 'TGTGTGTGTGGTGTG']])
            else:
                self.assertEqual(matches, [['test_6', 2, 12, 'GT', 'TGTGTGTGTG']])
        for min_repetitions in range(3, 6):
            for imperfect in range(1, 6):
                micro = utils.Microsatellite(length, min_repetitions=min_repetitions,
                                             imperfect=imperfect, strict=False, flanks=0,
                                             alphabet=alphabet)
                micro.load_sequence(('test_6', 'ACTGTGTGTGTGGTGTGAAGTGTCAACGTA'))
                matches = micro.find_microsatellites()
                if min_repetitions > 3:
                    self.assertEqual(matches, [['test_6', 2, 12, 'GT', 'TGTGTGTGTG']])
                elif imperfect < 2:
                    self.assertEqual(matches, [['test_6', 2, 17, 'GT', 'TGTGTGTGTGGTGTG']])
                else:
                    self.assertEqual(matches, [['test_6', 2, 23, 'GT', 'TGTGTGTGTGGTGTGAAGTGT']])

    def test_coordinates_different_kmers(self, length=2, alphabet='dna'):
        """Test if different kmers in the same positions result in the same coordinates"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_7', 'ACTGTGTGTGTGGTGTGAAGTGTCAACGTA'))
            matches_1 = micro.find_microsatellites()
            micro.load_sequence(('test_7', 'ACGTGTGTGTGTTGTGTAATGTGCAACTGA'))
            matches_2 = micro.find_microsatellites()
            self.assertEqual(matches_1[0][:3], matches_2[0][:3])

        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=0,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_7', 'ACTGTGTGTGTGGTGTGAAGTGTCAACGTA'))
            matches_1 = micro.find_microsatellites()
            micro.load_sequence(('test_7', 'ACGTGTGTGTGTTGTGTAATGTGCAACTGA'))
            matches_2 = micro.find_microsatellites()
            self.assertEqual(matches_1[0][:3], matches_2[0][:3])


class TestEdgeSequence(unittest.TestCase):

    def test_repeat_3_to_6_perfect_success(self, length=1, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_8', 'AAAAACATGAATAATAGAAGCAACGTGGGGGTGG'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [['test_8', 0, 5, 'A', 'AAAAA'],
                                       ['test_8', 26, 31, 'G', 'GGGGG']])

    def test_repeat_3_to_6_imperfect_success(self, length=1, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_9', 'AAAAACATGAATAATAGAAGCAACGTGGGGGTGG'))
            matches = micro.find_microsatellites()
            if min_repetitions == 3:
                self.assertEqual(matches, [['test_9', 0, 5, 'A', 'AAAAA'],
                                           ['test_9', 9, 14, 'A', 'AATAA'],
                                           ['test_9', 26, 34, 'G', 'GGGGGTGG']])
            else:
                self.assertEqual(matches, [['test_9', 0, 5, 'A', 'AAAAA'],
                                           ['test_9', 26, 31, 'G', 'GGGGG']])
        for min_repetitions in range(3, 4):
            for imperfect in range(2, 6):
                micro = utils.Microsatellite(length, min_repetitions,
                                             imperfect=imperfect,
                                             strict=False, flanks=0, alphabet=alphabet)
                micro.load_sequence(('test_9', 'AAAAACATGAATAATAGAAGCAACGTGGGGGTGG'))
                matches = micro.find_microsatellites()
                if min_repetitions == 3 and imperfect < 3:
                    self.assertEqual(matches, [['test_9', 0, 5, 'A', 'AAAAA'],
                                               ['test_9', 9, 14, 'A', 'AATAA'],
                                               ['test_9', 17, 23, 'A', 'AAGCAA'],
                                               ['test_9', 26, 34, 'G', 'GGGGGTGG']])
                elif min_repetitions == 3 and imperfect == 3:
                    self.assertEqual(matches, [['test_9', 0, 5, 'A', 'AAAAA'],
                                               ['test_9', 9, 23, 'A', 'AATAATAGAAGCAA'],
                                               ['test_9', 26, 34, 'G', 'GGGGGTGG']])
                elif min_repetitions == 3 and imperfect > 3:
                    self.assertEqual(matches, [['test_9', 0, 23, 'A', 'AAAAACATGAATAATAGAAGCAA'],
                                               ['test_9', 26, 34, 'G', 'GGGGGTGG']])
                else:
                    self.assertEqual(matches, [['test_9', 0, 5, 'A', 'AAAAA'],
                                               ['test_9', 26, 31, 'G', 'GGGGG']])

    def test_repeat_3_to_6_perfect_success_length_2(self, length=2, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_10', 'TATATATATTATATTATATATAT'))
            matches = micro.find_microsatellites()
            if min_repetitions <= 4:
                self.assertEqual(matches, [['test_10', 0, 9, 'AT', 'TATATATAT'],
                                           ['test_10', 14, 23, 'AT', 'TATATATAT']])
            else:
                self.assertEqual(matches, [])

    def test_repeat_3_to_6_perfect_success_length_3(self, length=3, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=False, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_11', 'TACTACTACTACTTACTACTTACTACTACTACTA'))
            matches = micro.find_microsatellites()
            if min_repetitions <= 4:
                self.assertEqual(matches, [['test_11', 0, 13, 'ACT', 'TACTACTACTACT'],
                                           ['test_11', 20, 34, 'ACT', 'TACTACTACTACTA']])
            else:
                self.assertEqual(matches, [])


class TestStrict(unittest.TestCase):

    def test_repeat_3_to_6_imperfect_strict(self, length=1, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=True, flanks=0, alphabet=alphabet)
            micro.load_sequence(('test_12', 'ACTGAAAAAGAAGCAACGTA'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [['test_12', 4, 9, 'A', 'AAAAA']])

    def test_repeat_3_to_6_imperfect_success(self, length=2, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            for imperfect in range(1, 6):
                micro = utils.Microsatellite(length, min_repetitions=min_repetitions,
                                             imperfect=imperfect, strict=True, flanks=0,
                                             alphabet=alphabet)
                micro.load_sequence(('test_13', 'ACTGTGTGTGTGGTGTGAAGTGTCAACGTA'))
                matches = micro.find_microsatellites()
                if min_repetitions > 3:
                    self.assertEqual(matches, [['test_13', 2, 12, 'GT', 'TGTGTGTGTG']])
                elif imperfect < 2:
                    self.assertEqual(matches, [['test_13', 2, 17, 'GT', 'TGTGTGTGTGGTGTG']])
                else:
                    self.assertEqual(matches, [['test_13', 2, 17, 'GT', 'TGTGTGTGTGGTGTG']])

        for min_repetitions in range(3, 6):
            for imperfect in range(1, 6):
                micro = utils.Microsatellite(length, min_repetitions=min_repetitions,
                                             imperfect=imperfect, strict=True, flanks=0,
                                             alphabet=alphabet)
                micro.load_sequence(('test_13', 'ACTGTGTGTGTGGTGTGGGGTGTCAACGTA'))
                matches = micro.find_microsatellites()
                if min_repetitions > 3:
                    self.assertEqual(matches, [['test_13', 2, 12, 'GT', 'TGTGTGTGTG']])
                elif imperfect < 2:
                    self.assertEqual(matches, [['test_13', 2, 17, 'GT', 'TGTGTGTGTGGTGTG']])
                else:
                    self.assertEqual(matches, [['test_13', 2, 23, 'GT', 'TGTGTGTGTGGTGTGGGGTGT']])

    def test_repeat_3_simple(self, length=2, alphabet='dna'):
        """Very simple test to see if the strict option works well"""
        micro = utils.Microsatellite(length, min_repetitions=3, imperfect=3,
                                     strict=True, flanks=0, alphabet=alphabet)
        micro.load_sequence(('test_14', 'ACTGTGTGTGTGGTGGTGTGAAGTGTCAACGTA'))
        matches = micro.find_microsatellites()
        self.assertEqual(matches, [['test_14', 2, 20, 'GT', 'TGTGTGTGTGGTGGTGTG']])

        micro = utils.Microsatellite(length, min_repetitions=3, imperfect=3,
                                     strict=True, flanks=0, alphabet=alphabet)
        micro.load_sequence(('test_14', 'ACTGTGTGTGTGGTGGAGTGAAGTGTCAACGTA'))
        matches = micro.find_microsatellites()
        self.assertEqual(matches, [['test_14', 2, 12, 'GT', 'TGTGTGTGTG']])


class TestFlanks(unittest.TestCase):

    def test_repeat_3_to_6_imperfect_strict_flanks_in(self, length=1, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=True, flanks=5, alphabet=alphabet)
            micro.load_sequence(('test_15', 'AGACCTGAAAAAGAAGCAACGTA'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [['test_15', 7, 12, 'A', 'AAAAA', 'ACCTG', 'GAAGC']])

    def test_repeat_3_to_6_imperfect_strict_flanks_out(self, length=1, alphabet='dna'):
        """Test succeeds because the microsatellites are long enough"""
        for min_repetitions in range(3, 6):
            micro = utils.Microsatellite(length, min_repetitions, imperfect=-1,
                                         strict=True, flanks=10, alphabet=alphabet)
            micro.load_sequence(('test_16', 'AGACCTGAAAAAGAAGCAACGTA'))
            matches = micro.find_microsatellites()
            self.assertEqual(matches, [['test_16', 7, 12, 'A', 'AAAAA', 'AGACCTG', 'GAAGCAACGT']])


if __name__ == "__main__":
    unittest.main()
